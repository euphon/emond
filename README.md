# emond

您好！欢迎使用又枫科技研发的 EMOND 产品，该产品可以帮助您实时监测服务器运行状况，确保其运行正常。
# EMOND 是什么？
EMOND 是又枫科技研发的一款灵活性极高的服务器监控工具，使用 Python + Flask 提供丰富的 API 接口，并将数据上传至 Influx 服务器，并提供基于时间序列数据的可视化。EMOND 提供包括磁盘，网络，CPU，内存，GPU等硬件指标的监控，并且可以个性化拓展。
# 我们希望谁来使用？
EMOND 是用于服务器运行状况监测的工具，若您需要管理服务器并希望实时检查其运行情况，EMOND 是您的不二之选。
# 如何使用 EMOND ？
我们提供了四个程序，分别对应EMOND的安装（install），运行（run），访问（mon），停止（stop）
首次使用时，请先安装EMOND，在服务器命令行内执行以下代码：
git clone https://gitlab.com/euphon/emond.git
之后运行如下代码：
cd emond
bash install
注意您可能需要在必要的地方输入服务器的密码以获取root权限，并且同意安装运行EMOND所需的前置软件包
之后运行flask本地服务器，并默认以每分钟的频率收集数据并上传至influxdb服务器：
bash run
按下ctrl+c停止收集数据
