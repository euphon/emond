#!/usr/bin/env python3
from flask import Flask
from flask import request
from markupsafe import escape
import os
import re
import time
import pynvml
import json

from datetime import datetime

from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

# You can generate a Token from the "Tokens Tab" in the UI
token = "qsFZvRbXp2GWabr81okpHdKajcharqio3LSkM2Vg0Axj4C0EoDrWniRHLviZl9W37rVJoDygzjU1O3UqyO9qlw=="
org = "Euphon"
bucket = "euphon_emond"

client = InfluxDBClient(url="https://influx.euphon.uk", token=token)
write_api = client.write_api(write_options=SYNCHRONOUS)

app = Flask(__name__)


deviceProperties = {
    "Seagate": {
        "Average_life_hours": 2000000,
        "Average_write_gb": 20000 * 1024, # TBW calculated as TBW=(SSD capacity)×(Program / Erase cycles of NAND)/(Write Amplification Factor), where SSD capacity = 8TB, 
        "Average_half_life": 2000000 / 2,                                                                   # Program / Erase cycles of NAND = 5000 (assumed)
        "Average_half_write_gb": 20000 * 1024 / 2,                                                          # Write Amplification Factor = 2 (assumed)
    },
}

@app.route('/getAllDev')
def getAllDev():
    try:
        # run lsblk to get all the devices
        lsblkCMD = "lsblk"
        result = os.popen(lsblkCMD).readlines()
        # print('result', result)
        # match the following devices: sata, nvme, sd[a-z], vd[a-z] using regex
        presentDevices = []
        for line in result:
            # check if 'sata' is in the line
            if 'sata' in line:
                presentDevices.append('sata')
            # check if 'nvme' is in the line
            if 'nvme' in line:
                presentDevices.append('nvme')
            # check if 'sda', 'sdb', 'sdc', ... , 'sdz' is in the line
            for i in range(26):
                if 'sd' + chr(ord('a') + i) in line and 'sd' + chr(ord('a') + i) not in presentDevices:
                    presentDevices.append('sd' + chr(ord('a') + i))
            # check if 'vda', 'vdb', 'vdc', ... , 'vdz' is in the line
            for i in range(26):
                if 'vd' + chr(ord('a') + i) in line and 'vd' + chr(ord('a') + i) not in presentDevices:
                    presentDevices.append('vd' + chr(ord('a') + i))
        # print(presentDevices)
        returnDict = {}
        IOSTATCMD = "iostat -xd -o JSON"
        for deviceName in presentDevices:
            # convert above to fstring
            deviceNameEx = f"/dev/{deviceName}"
            powerOnHoursCMD = f"smartctl -a {deviceNameEx} | grep Power_On_Hours | awk '{{print $10}}'"
            restSpaceCMD = f"df -h | grep {deviceNameEx} | awk '{{print $1, $4}}'"
            totalReadLBA = f"smartctl -a {deviceNameEx} | grep Total_LBAs_Read | awk '{{print $10}}'"
            totalWriteLBA = f"smartctl -a {deviceNameEx} | grep Total_LBAs_Written | awk '{{print $10}}'"
            # BWCMD_1 = f"iostat -x {deviceName} | grep {deviceName} | awk '{{ print $3; }}'"
            # BWCMD_2 = f"iostat -x {deviceName} | grep {deviceName} | awk '{{ print $9; }}'"
            # IOPSCMD_1 = f"iostat -dx | grep {deviceName} | awk '{{print $2; }}'"
            # IOPSCMD_2 = f"iostat -dx | grep {deviceName} | awk '{{print $8; }}'"
            # latencyCMD = f"iostat -s -x | grep {deviceName} | awk '{{ print $5; }}'"
            cmdList = [("power_on_hours", powerOnHoursCMD), ("rest_space", restSpaceCMD), ("total_read_LBA_bytes", totalReadLBA),
                    ("total_write_LBA_bytes", totalWriteLBA)]
            result = []
            for cmd in cmdList:
                if re.findall('df -h', cmd[1]):
                    v = str(os.popen(cmd[1]).readlines())[2: -4].replace('\\n', '')
                    if v == '':
                        continue
                    v = v.replace('\'', '').split(',')
                    t_dict = {}
                    for s in v:
                        s = s.strip().split(' ')
                        t_dict[s[0]] = s[1]
                    result.append((cmd[0], t_dict))
                else:
                    v = str(os.popen(cmd[1]).readlines())[2: -4]
                    try:
                        v = int(v)
                        if re.findall('LBA', cmd[1]):
                            v = v * 512 # convert to bytes, assuming 512 bytes per sector
                    except:
                        try:
                            v = float(v)
                        except:
                            pass
                    result.append((cmd[0], v))
            # result = ("result: ", ','.join(os.popen("ls").readlines()))
            # print(type(result))
            # print(dict(result))
            returnDict[deviceName] = dict(result)

            # perform iostat cmd and get read_kbps, write_kbps, read_iops, write_iops, avg_latency for each device
            r = os.popen(IOSTATCMD).readlines()
            e = ''.join(r)
            # print(e)
            # convert to json
            r = json.loads(e)
            
            diskInfo = r["sysstat"]["hosts"][0]["statistics"][0]["disk"]
            # print(diskInfo)
            for info in diskInfo:
                if info['disk_device'] == deviceName:
                    returnDict[deviceName].update({
                        "write_kbps": info['wkB/s'],
                        "read_kbps": info['rkB/s'],
                        "write_iops": info['w/s'],
                        "read_iops": info['r/s'],
                        "write_latency": info['w_await'],
                        "read_latency": info['r_await'],
                        })
                    break
        
            devicePath = f"/dev/{deviceName}"
            statvfs = os.statvfs(devicePath)

            total_inodes = statvfs.f_files
            free_inodes = statvfs.f_ffree
            available_inodes = statvfs.f_favail

            returnDict[deviceName].update({
                "total_inodes": total_inodes,
                "free_inodes": free_inodes,
                "available_inodes": available_inodes,
            })


        # print(returnDict)
        return returnDict
    except Exception as e:
        # print(e)
        return str(e) + '\n'
        

@app.route('/lifeEst')
def lifeEst():
    try:
        mode = request.args.get('mode', 'linear').capitalize()
        stat = getAllDev()
        allDevs = stat.keys()
        AllDevsLife = {}
        for deviceName in allDevs:
            deviceName = 'Seagate'     # DELETE ME in the future
            powerUpHours = stat[deviceName]["power_on_hours"]
            totalWriteGB = (float(stat[deviceName]["total_write_LBA_bytes"]) / 1024 / 1024 / 1024) if stat[deviceName]["total_write_LBA_bytes"] != '' else 0
            print(f"{deviceName} powerUpHours: {powerUpHours}, totalWriteGB: {totalWriteGB}")
            if mode == 'Linear':
                # Power_life_percent = (Average_life_hours - Total_power_up_hours) / Average_life_hours * 100%
                power_life_percent = (deviceProperties[deviceName]['Average_life_hours'] - powerUpHours) / deviceProperties[deviceName]['Average_life_hours'] * 100
                # Data_write_life_percent = (Average_write_gb - Total_write_gb) / Average_write_gb * 100%
                data_write_life_percent = (deviceProperties[deviceName]['Average_write_gb'] - totalWriteGB) / deviceProperties[deviceName]['Average_write_gb'] * 100
                # Remaining_life_percent = MIN(Power_life_percent, Data_write_life_percent)
                remaining_life_percent = min(power_life_percent, data_write_life_percent)
                AllDevsLife[deviceName] = remaining_life_percent
            elif mode == 'Halflife':
                # N = N0 * (1 / 2) ^ (t / T)
                expected_life_hours_percent = (1 / 2) ** (powerUpHours / deviceProperties[deviceName]['Average_half_life'])
                expected_write_gb_percent = (1 / 2) ** (totalWriteGB / deviceProperties[deviceName]['Average_half_write_gb'])
                remaining_life_percent = min(expected_life_hours_percent, expected_write_gb_percent)
                AllDevsLife[deviceName] = remaining_life_percent * 100
        return AllDevsLife
    except Exception as e:
        # print(e)
        return str(e)

@app.route('/getNetworkInfo')
def getNetworkInfo():
    try:
        res = {}
        getPortListCMD = "ip link"
        portList = {}
        index = 1
        for  line in os.popen(getPortListCMD).readlines():
            # print(line)
            t = line.split(':')
            if t[0] == str(index):
                portList[t[0].strip()] = t[1].strip()
                index += 1
        res['port_list'] = portList
        getIPSubnetMaskCMD = "ip addr"
        ipSubnetMask = {}
        index = 1
        for line in os.popen(getIPSubnetMaskCMD).readlines():
            # print(line)
            if line.strip().split(' ')[0] == 'inet':
                t = line.strip().split(' ')[1]
                ip = t.split('/')[0]
                subnetMask = t.split('/')[1]
                ipSubnetMask[str(index)] = {'port': portList[str(index)], 'ip': ip, 'subnet_mask': subnetMask}
                index += 1
        res['ip_subnet_mask'] = ipSubnetMask
        getDriverCMD = lambda x: 'ethtool -i' + ' ' + x
        driverType = {}
        # print(portList)
        for dev in portList.values():
            if dev == 'lo':
                continue
            driver = os.popen(getDriverCMD(dev)).readlines()[0].split(':')[1].strip()
            driverType[dev] = driver
        res['driver_type'] = driverType
        getIPRouteCMD = "ip route"
        ipRoute = ''.join(os.popen(getIPRouteCMD).readlines()).replace('\n', '').strip()
        res['ip_route'] = ipRoute
        # need root
        # getSpeedCMD = lambda x: 'ethtool ' + x + ' | grep Speed'
        # speed = {}
        # for dev in portList.values():
        #     if dev == 'lo':
        #         continue
        #     speed[dev] = os.popen(getSpeedCMD(dev)).readlines()[0].split(':')[1].strip()
        # res['dev_speed'] = speed
        # getOpenPortCMD = "netstat -plnt | awk '{print $4; }'"
        # openPort = []
        # for line in os.popen(getOpenPortCMD).readlines()[2:]:
        #     # openPort.append(line.strip().split(':')[-1])
        #     # append nonrepeated port
        #     t = line.strip().split(':')[-1]
        #     if t not in openPort:
        #         openPort.append(t)
        # res['open_port'] = openPort
        getInternetSpeedCMD = "ifstat -a 10 1"
        internetSpeed = {}
        res1 = os.popen(getInternetSpeedCMD).readlines()
        devs = res1[0].strip().split(' ')
        devs = [x for x in devs if x]
        stat = res1[2].strip().split(' ')
        stat = [x for x in stat if x]
        for i in range(0, len(devs), 1):
            internetSpeed[devs[i]] = {'in': float(stat[2 * i]), 'out': float(stat[2 * i + 1])}
        res['internet_speed'] = internetSpeed
        getLinkStatCMD = lambda dev: 'ip -s link show' + ' ' + dev
        linkStat = {}
        for dev in portList.values():
            if dev == 'lo':
                continue
            t = {}
            # find bytes, packets, errors, dropped, missed, mcast, each for RX and TX
            re = os.popen(getLinkStatCMD(dev)).readlines()
            RXstat = re[3].strip().split(' ')
            # remove empty string
            RXstat = [int(x) for x in RXstat if x]
            t['RX'] = {'bytes': RXstat[0], 'packets': RXstat[1], 'errors': RXstat[2], 'dropped': RXstat[3], 'missed': RXstat[4], 'mcast': RXstat[5]}
            TXstat = re[5].strip().split(' ')
            TXstat = [int(x) for x in TXstat if x]
            t['TX'] = {'bytes': TXstat[0], 'packets': TXstat[1], 'errors': TXstat[2], 'dropped': TXstat[3], 'missed': TXstat[4], 'mcast': TXstat[5]}
            linkStat[dev] = t
        res['link_stat'] = linkStat
        getTCPconnectionCMD = "ss -t -a"
        res['active_TCP_connection'] = len(os.popen(getTCPconnectionCMD).readlines()[1:])
        
        return res
    except Exception as e:
        return str(e)

@app.route('/getGPUInfo')
def getGPUInfo():
    # Initialize NVML
    try:
        info = {}
        pynvml.nvmlInit()
        handle = pynvml.nvmlDeviceGetHandleByIndex(0)
        # get GPU name
        info['name'] = pynvml.nvmlDeviceGetName(handle)
        # Memory Utilization
        meminfo = pynvml.nvmlDeviceGetMemoryInfo(handle)
        info['mem_total_bytes'] = meminfo.total
        info['mem_used_bytes'] = meminfo.used
        info['mem_free_bytes'] = meminfo.free
        # GPU Utilization
        util = pynvml.nvmlDeviceGetUtilizationRates(handle)
        info['gpu_util'] = util.gpu
        info['mem_util'] = util.memory
        # GPU temperature
        info['temperature'] = pynvml.nvmlDeviceGetTemperature(handle, pynvml.NVML_TEMPERATURE_GPU)
        # Power Usage and Limit
        power = pynvml.nvmlDeviceGetPowerUsage(handle)
        info['power_usage'] = power
        power_limit = pynvml.nvmlDeviceGetEnforcedPowerLimit(handle)
        info['power_limit'] = power_limit
        # Fan Speed
        fan = pynvml.nvmlDeviceGetFanSpeed(handle)
        info['fan_speed'] = fan
        # PCI utilization
        pci = pynvml.nvmlDeviceGetPcieThroughput(handle, pynvml.NVML_PCIE_UTIL_TX_BYTES)
        info['pci_util_TX_bps'] = pci
        pci = pynvml.nvmlDeviceGetPcieThroughput(handle, pynvml.NVML_PCIE_UTIL_RX_BYTES)
        info['pci_util_RX_bps'] = pci
        # Clock Speeds
        clock = pynvml.nvmlDeviceGetClockInfo(handle, pynvml.NVML_CLOCK_GRAPHICS)
        info['clock_graphics'] = clock
        clock = pynvml.nvmlDeviceGetClockInfo(handle, pynvml.NVML_CLOCK_SM)
        info['clock_sm'] = clock
        clock = pynvml.nvmlDeviceGetClockInfo(handle, pynvml.NVML_CLOCK_MEM)
        info['clock_mem'] = clock
        # number of Running Processes
        info['processes'] = len(pynvml.nvmlDeviceGetComputeRunningProcesses(handle))
        # number of Running Graphics Processes
        info['graphics_processes'] = len(pynvml.nvmlDeviceGetGraphicsRunningProcesses(handle))

        # Performance State
        p_state = pynvml.nvmlDeviceGetPerformanceState(handle)
        # print("Performance State:", p_state)

        # Shutdown NVML
        pynvml.nvmlShutdown()
        return info

    except Exception as e:
        # print(e)
        return None

@app.route('/getCPUInfo')
def getCPUInfo():    
    
    # initialize an empty dictionary to store the metrics
    metrics = {}

    # Open the /proc/cpuinfo file. This file contains CPU hardware information.
    with open("/proc/cpuinfo", "r") as f:
        info = f.readlines()
    # print(info)
    # Count the number of processor cores
    metrics["cpu_cores"] = len([i for i in info if "processor" in i])
    
    # Retrieve the vendor of the CPU
    metrics["cpu_vendor"] = [i for i in info if "vendor_id" in i][0].split(":")[1].strip()

    # Retrieve the model name of the CPU
    metrics["cpu_model"] = [i for i in info if "model name" in i][0].split(":")[1].strip()

    # Retrieve the operating frequency of the CPU (in MHz)
    metrics["cpu_mhz"] = float([i for i in info if "cpu MHz" in i][0].split(":")[1].strip())

    # Retrieve the size of the cache of the CPU
    metrics["cache_size_KB"] = int([i for i in info if "cache size" in i][0].split(":")[1].strip().split(" ")[0])

    # Retrieve the family of the CPU
    metrics["cpu_family"] = int([i for i in info if "cpu family" in i][0].split(":")[1].strip())

    # Retrieve the stepping of the CPU
    metrics["cpu_stepping"] = [i for i in info if "stepping" in i][0].split(":")[1].strip()

    # Open the /proc/stat file. This file contains CPU and system statistics.
    with open("/proc/stat", "r") as f:
        info = f.readlines()
    # print(info)
    # Retrieve various CPU times
    cpu_stat = info[0].strip().split(" ")[1:]
    metrics["user_time_jiffies"] = int(cpu_stat[0] or 0)
    metrics["nice_time_jiffies"] = int(cpu_stat[1] or 0)
    metrics["system_time_jiffies"] = int(cpu_stat[2] or 0)
    metrics["idle_time_jiffies"] = int(cpu_stat[3] or 0)
    metrics["iowait_time_jiffies"] = int(cpu_stat[4] or 0)
    metrics["irq_time_jiffies"] = int(cpu_stat[5] or 0)
    metrics["softirq_time_jiffies"] = int(cpu_stat[6] or 0)

    # Open the /proc/loadavg file. This file contains system load average information.
    with open("/proc/loadavg", "r") as f:
        info = f.readline().strip().split()

    # print(info)
    # Retrieve the system load averages for the past 1, 5, and 15 minutes
    metrics["1min_load"] = float(info[0] or 0) 
    metrics["5min_load"] = float(info[1] or 0) 
    metrics["15min_load"] = float(info[2] or 0)

    # Retrieve the number of currently running processes and the total number of processes
    metrics["running_processes"], metrics["total_processes"] = map(int, info[3].split('/'))

    # Return the metrics dictionary
    return metrics

@app.route('/getMemoryInfo')
def getMemoryInfo():
    # Initialize the dictionary to hold the metrics
    memory_info = {}

    # Open the file at /proc/meminfo, which contains information about the system's memory usage
    with open('/proc/meminfo', 'r') as f:
        for line in f.readlines():
            # Each line in this file is a single metric, in the format "MetricName: Value kB"
            name, value = line.split(":")
            # Convert the value to integer and remove the 'kB' from the end
            value = int(value.strip().split(' ')[0])
            # Store this metric in the dictionary
            memory_info[name] = value

    # Open the file at /proc/vmstat, which contains more detailed memory statistics
    with open('/proc/vmstat', 'r') as f:
        for line in f.readlines():
            # Each line in this file is a single metric, in the format "MetricName Value"
            name, value = line.strip().split()
            # Convert the value to integer
            value = int(value)
            # Store this metric in the dictionary
            memory_info[name] = value

    return memory_info

    
@app.route('/dbsave')
def dbsave():
    
    getHostNmae = "hostname"
    hostName = os.popen(getHostNmae).readlines()[0].strip()
    # print("EMOND is running, press Ctrl+C to stop monitoring\n")
    
    try:
        # save disk info to database every 5 minutes
        diskInfo = getAllDev()
        disk = 'disk'
        for dev in diskInfo.keys():
            tag1 = dev
            for info in diskInfo[dev].keys():
                if info != 'rest_space':
                    data = diskInfo[dev][info]
                    if not data:
                        data = 0.0
                    data = float(data)
                    # print(data)
                    point = Point(disk).tag('dev', tag1).tag('host', hostName).field(info, data).time(datetime.utcnow(), WritePrecision.NS)
                    write_api.write(bucket, org, point)
                else:
                    for part in diskInfo[dev][info].keys():
                        tag2 = part
                        space = diskInfo[dev][info][part]
                        if space[-1] == 'G':
                            space = space[:-1]
                            space = float(space)
                        elif space[-1] == 'M':
                            space = space[:-1]
                            space = float(space) / 1024
                        point = Point(disk).tag('dev', tag1).tag('part', tag2).tag('host', hostName).field('rest_space', space).time(datetime.utcnow(), WritePrecision.NS)
                        write_api.write(bucket, org, point)
    except Exception as e:
        # print(e)
        raise Exception("Error when saving disk info: " + str(e))

    try:
        # save network info to database every 5 minutes
        networkInfo = getNetworkInfo()
        network = 'network'
        for netAttr in networkInfo.keys():
            if netAttr == 'active_TCP_connection':
                point = Point(network).tag("host", hostName).field('active_TCP_connection', networkInfo[netAttr]).time(datetime.utcnow(), WritePrecision.NS)
                write_api.write(bucket, org, point)
            elif netAttr == 'internet_speed':
                netStat = networkInfo[netAttr]
                for dev in netStat.keys():
                    point = Point(network).tag('dev', dev).tag('host', hostName).field('in', netStat[dev]['in']).field('out', netStat[dev]['out']).time(datetime.utcnow(), WritePrecision.NS)
                    write_api.write(bucket, org, point)
            elif netAttr == 'link_stat':
                for dev in networkInfo[netAttr].keys():
                    tag1 = dev
                    for stat in networkInfo[netAttr][dev].keys():
                        tag2 = stat
                        for subStat in networkInfo[netAttr][dev][stat].keys():
                            data = networkInfo[netAttr][dev][stat][subStat]
                            if not data:
                                data = 0.0
                            data = float(data)
                            point = Point(network).tag('port', tag1).tag('TX/RX', tag2).tag('host', hostName).field(subStat, data).time(datetime.utcnow(), WritePrecision.NS)
                            write_api.write(bucket, org, point)
    except Exception as e:
        # print(e)
        raise Exception("Error when saving network info: " + str(e))

    try:
        # save GPU info to database every 5 minutes
        gpuInfo = getGPUInfo()
        gpu = 'gpu'
        gpuName = gpuInfo['name']
        for gpuAttr in gpuInfo.keys():
            if gpuAttr == 'name':
                continue
            point = Point(gpu).tag('GPU_Name', gpuName).tag('host', hostName).field(gpuAttr, gpuInfo[gpuAttr]).time(datetime.utcnow(), WritePrecision.NS)
            write_api.write(bucket, org, point)

    except Exception as e:
        # print(e)
        if "NoneType" in str(e):
            print("No GPU detected\n")
        else:
            raise Exception("Error when saving GPU info: " + str(e))

    try:
        # save CPU info to database every 5 minutes
        cpuInfo = getCPUInfo()
        cpu = 'cpu'
        cpuModel = cpuInfo['cpu_model']
        for cpuAttr in cpuInfo.keys():
            if cpuAttr == 'cpu_model':
                continue
            point = Point(cpu).tag('host', hostName).tag("cpu_model", cpuModel).field(cpuAttr, cpuInfo[cpuAttr]).time(datetime.utcnow(), WritePrecision.NS)
            write_api.write(bucket, org, point)
    except Exception as e:
        # print(e)
        raise Exception("Error when saving CPU info: " + str(e))

    try:
        # save memory info to database every 5 minutes
        memoryInfo = getMemoryInfo()
        memory = 'memory'
        for memoryAttr in memoryInfo.keys():
            point = Point(memory).tag('host', hostName).field(memoryAttr, memoryInfo[memoryAttr]).time(datetime.utcnow(), WritePrecision.NS)
            write_api.write(bucket, org, point)
    except Exception as e:
        # print(e)
        raise Exception("Error when saving memory info: " + str(e))
    
    # yield a prompt to show the program is still running and current time
    return '\nsuccess:' + ' ' + str(datetime.now()) + '\n'


